let Agent = require('socks5-http-client/lib/Agent');
let AgentS = require('socks5-https-client/lib/Agent');
let request = require('request');

// testHttp()
testHttps()


function testHttp(){
    request({
        url: 'http://en.wikipedia.org/wiki/SOCKS',
        agentClass: Agent,
        agentOptions: {
            socksHost: '127.0.0.1',
            socksPort: 1080,
            socksUsername: 'user',
            socksPassword: 'pass',
        }
    }, function(err, res) {
        console.log(err || res.body);
    });
}

function testHttps() {
    request({
        url: 'https://github.com',
        // strictSSL: true,
        agentClass: AgentS,
        agentOptions: {
            socksHost: '127.0.0.1',
            socksPort: 1080,
            socksUsername: 'user',
            socksPassword: 'pass',
        }
    }, function(err, res) {
        console.log(err || res.body);
    });
}


