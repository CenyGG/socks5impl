const net = require('net');
const http = require("http");

class Socks5 {
    constructor() {
        this.ver = 0x05;
        this.port = 1080;
        this.supportedMethods = [0x02, 0x00]                //user-pass, no auth
        this.supportedCmds = [0x01]                         //connect only
        this.supportedAddrTypes = [0x01, 0x03, 0x04]        //ip4 , domain, ip6

        const server = net.createServer()
            .on('connection', (socket) => {
                log(`New connection: ${socket.remoteAddress}:${socket.remotePort}`);

                socket
                    .once('data', (data) => {
                        if (data[0] !== this.ver) closeSocketBad(socket)
                        switch (this.chooseMethod(data)) {
                            case 0x00:
                                socket.write(new Buffer([0x05, 0x00]))
                                this.handleConnection(socket)
                                break
                            case 0x02:
                                socket.write(new Buffer([0x05, 0x02]))
                                this.handleAuthentication(socket)
                                break
                            default:
                                closeSocketBad(socket)
                                break
                        }
                    })
                    .on('error', (err) => {
                        log("Socket error: ", err.message)
                    })
            })
            .on('error', (err) => {
                log("Server error: ", err.message)
                server.close()
                server.listen(this.port, () => {
                    log(`Server restarted on port ${this.port}`)
                })
            })

        server.listen(this.port, () => {
            log(`Server starts on port ${this.port}`);
        })
    }

    handleAuthentication(socket) {
        socket.once('data', (data) => {
            const nameLength = data[1]
            const passLength = data[nameLength + 2]
            const name = data.slice(2, 2 + nameLength).toString('utf-8')
            const pass = data.slice(nameLength + 3, nameLength + passLength + 3).toString('utf-8')

            if (!validatePassword(name, pass)) return closeSocketBad(socket, new Buffer([0x05, 0xFF]))

            socket.write(new Buffer([0x01, 0x00]))
            this.handleConnection(socket)
        })
    }

    handleConnection(socket) {
        socket.once('data', (data) => {
            const cmd = data[1]
            if (!this.supportedCmds.includes(cmd)) return closeSocketBad(socket)

            const adrType = data[3]
            if (!this.supportedAddrTypes.includes(adrType)) return closeSocketBad(socket)

            let dstAdr = null
            let dstPort = null

            const chunk1 = new Buffer([0x05, 0x00, 0x00, adrType])
            let successBuff;

            switch (adrType) {
                case 0x01:
                    dstAdr = data.slice(4, 8)
                    dstPort = normalizePort(data.slice(8, 10))
                    successBuff = Buffer.concat([chunk1, dstAdr, dstPort], chunk1.length + dstAdr.length + dstPort.length)
                    break
                case 0x03:
                    const nameLength = data[4]
                    dstAdr = data.slice(5, 5 + nameLength)
                    dstPort = normalizePort(data.slice(6 + nameLength, 8 + nameLength))

                    const chunk2 = new Buffer([dstAdr.length])
                    successBuff = Buffer.concat([chunk1, chunk2, dstAdr, dstPort], chunk1.length + chunk2.length + dstAdr.length + dstPort.length)
                    break
                case 0x04:
                    dstAdr = data.slice(4, 20)
                    dstPort = data.slice(20, 22)
                    break
            }
            if (!(dstAdr && dstPort)) return closeSocketBad(socket)

            this.createProxy(dstAdr, dstPort, adrType, socket, successBuff)
        })
    }

    createProxy(dstAdr, dstPort, type, socket, successBuff) {
        const port = dstPort.length === 1 ? dstPort.readUInt8(0) : dstPort.readUInt16BE(0)
        let options;

        switch (type) {
            case 0x01:
                options = {
                    hostname: `${dstAdr[0]}.${dstAdr[1]}.${dstAdr[2]}.${dstAdr[3]}`,
                    port: port
                };

                return this.proxyData(socket, options, successBuff)
            case 0x03:
                options = {
                    hostname: dstAdr.toString('utf-8'),
                    port: port
                };

                return this.proxyData(socket, options, successBuff)
            case 0x04:
                //v6 stuff
                break;
        }
    }

    proxyHttpData(socket, httpOptions) {
        http.request(httpOptions)
            .on('socket', (remoteSocket) => {
                log(`Connected to destination host ${httpOptions.hostname} : ${httpOptions.port}`)
                handleClosing(socket, remoteSocket)

                socket.pipe(remoteSocket)
                remoteSocket.pipe(socket)
            }).on('error', (err) => {
            log(err)
        })
    }

    proxyData(socket, options, successBuff) {
        const remoteSocket = net.connect(443, options.hostname, () => {
            log(`Connected to destination host ${options.hostname} : ${options.port}`)
            socket.write(successBuff)
            handleClosing(socket, remoteSocket)

            socket.pipe(remoteSocket)
            remoteSocket.pipe(socket)
        })
        remoteSocket.setTimeout(3000);

        remoteSocket
            .on('timeout', () => {
                console.log('socket timeout');
                socket.write(new Buffer([0x05, 0x04]))   //not available
                socket.end();
            })
    }

    chooseMethod(data) {
        return 0x02
        for (let i = 2; i < data.length; i++) {
            if (this.supportedMethods.includes(data[i])) {
                return data[i]
            }
        }
    }
}

function closeSocketBad(socket, res) {
    const badRes = res || new Buffer([0x05, 0xFF])
    socket.end(badRes)
    socket.destroy()
}

function validatePassword(user, pass) {
    const tempUser = "user";
    const tempPass = "pass";
    return !(user !== tempUser || pass !== tempPass);
}

function normalizePort(port) {
    if (port.length === 1) {
        let tempBuff = Buffer.alloc(2)
        tempBuff.writeInt16BE(port.readUInt8(0), 0)
        port = tempBuff
    }
    return port
}

function log(msg){
    console.log(msg)
}

function handleClosing(clientSocket, proxySocket) {
    function close(from, to) {
        from
            .on('end', () => {
                log(`end ${from.remoteAddress}:${from.remotePort}`)
                to.destroy()
            })
            .on('close', () => {
                log(`close ${from.remoteAddress}:${from.remotePort}`)
                to.destroy()
            })
            .on('error', (err) => {
                log(`error ${from.remoteAddress}:${from.remotePort}`)
                to.destroy(err)
            })
    }

    close(clientSocket, proxySocket)
    close(proxySocket, clientSocket)
}

module.exports.Socks5 = Socks5;